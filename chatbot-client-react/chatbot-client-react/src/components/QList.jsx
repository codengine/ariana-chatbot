import React, {Component} from "react"
import Pagination from "./Pagination"
import QItem from "./QItem"


const mapStateToProps = state => ({
    ...state.QListReducer,
    qListItems: state.AppReducer.qListItems
});

const mapDispatchToProps = dispatch => ({
  onQItemClick: (qThreadId) =>
    dispatch({type: "QITEM_CLICKED", qThreadId: qThreadId, initialLoad: true}),
  onLoad: (qListItems) =>
    dispatch({ type: "APP_LOADED", qListItems: qListItems }),
  onUnload: () =>
    dispatch({  type: "APP_UNLOADED" })
});

class QList extends Component{

    render(){
        return (
            <React.Fragment>
                <ul className={this.props.ulClassName}>
                    {this.state.qList.map(qItem => (
                        <QItem key={qItem.id} id={qItem.id} qItemClickHandler={this.props.qItemClickHandler} className={this.props.liClassName} text={qItem.text}/>
                    ))}
                </ul>
                <Pagination></Pagination>
            </React.Fragment>
        )
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(QList);