import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from "./Header"
import Pagination from "./Pagination"
import QItem from "./QItem"
import QResponsePostForm from "./QResponsePostForm"
import RestAPI from "../api/RestAPI"
import SafeLogger from "../core/SafeLogger"
import SafeLocalStorage from "../core/SafeLocalStorage"
// import {APP_LOADED, APP_UNLOADED, QITEM_CLICKED} from "../actions/ActionTypes"

import '../Style.css';


const mapStateToProps = state => ({
    ...state.QListReducer,
    appLoaded: state.AppReducer.appLoaded,
    clientId: state.AppReducer.clientId,
    currentQThread: state.QThreadReducer.currentQThreadId,
    lastConversationQId: state.QThreadReducer.lastConversationQId,
    qListItems: state.QListReducer.qListItems,
    count: state.QListReducer.count, 
    currentPage: state.QListReducer.currentPage,
    previous: state.QListReducer.previous, 
    next: state.QListReducer.next, 
    error: state.QListReducer.error,
    qItem: state.QListReducer.qItem,
    cList: state.QThreadReducer.items,
    qThreadReady: state.QThreadReducer.qThreadReady
});

const mapDispatchToProps = dispatch => ({
  attachLogger: loggerInstance => 
    dispatch({ type: "ATTACH_LOGGER", loggerInstance: loggerInstance }),
  attachLocalStorage: storageInstance => 
    dispatch({ type: "ATTACH_LOCAL_STORAGE", storageInstance: storageInstance }),
  attachClientId: clientId => 
    dispatch({ type: "ATTACH_CLIENT_ID", clientId: clientId }),
  onQItemClick: (qThreadId, messageFrom, hasNext, cId, qText, qParentId, error, message, rowClassName) =>
    dispatch({type: "QITEM_CLICKED", qThreadId, messageFrom, initialLoad: true, 
        hasNext, cId, qText, qParentId, error, message, rowClassName}),
  onLoad: (count, qListItems, previous, next, errors, clientId) =>
    dispatch({ type: "APP_LOADED", count, qListItems, previous, next, errors, clientId }),
  onUnload: () =>
    dispatch({  type: "APP_UNLOADED" })
});

class ChatbotWrapper extends Component {

    componentWillMount() {
        const loggerInstance = new SafeLogger();
        const localStorageInstance = new SafeLocalStorage();
        this.props.attachLogger(loggerInstance);
        this.props.attachLocalStorage(localStorageInstance);

        let clientId = localStorageInstance.getClientId();

        this.props.attachClientId(clientId);        

        let api = new RestAPI();
        api.fetchQList().then(response=>{
            console.log(response);
            let count = response.count;
            let qItems = response.results;
            let previous = response.previous;
            let next = response.next;
            let clientId = response.clientId;
            this.props.onLoad(count, qItems, previous, next, null, clientId);
        }).catch(error=>{
            console.log(error);
            this.props.onLoad(-1, null, null, null, error, clientId);
        })
    }

    loadQItem = qThreadId => {
        let api = new RestAPI();
        api.loadNextQ(qThreadId, null).then(response=>{
            console.log(response);
            let conversationId = null;
            let text = null;
            let parentId = null;
            let messageFrom = "Ariana: ";
            let rowClassName = "ariana-chat-row";
            if(response.object != null){
                conversationId = response.object.id;
                text = response.object.text;
                parentId = response.object.parent_id;
            }
            this.props.onQItemClick(qThreadId, messageFrom, response.has_next, conversationId, text, parentId, null, response.message, rowClassName);
        }).catch(error=>{
            let conversationId = null;
            let text = null;
            let parentId = null;
            let messageFrom = "System: ";
            let rowClassName = "system-chat-row";
            console.log(error);
            this.props.onQItemClick(qThreadId, messageFrom, null, conversationId, text, parentId, error, "Something went wrong!", rowClassName);
        })
    }

    componentWillUnmount() {
      this.props.onUnload();
    }

  renderConversationBody = cList => {
    if(cList == null){
        return <div>Please select a questionairre to start</div>;
    }
    else{
        if(cList.length > 0){
            console.log(cList);
            return cList.map((item, index) => (
                <div key={ index } className={item.rowClassName}><span>{item.messageFrom}</span> <span>{item.qText}</span></div>
            ))
        }
        else{
            return <div>Please select a questionairre to start</div>;
        }
    }
    
  }

  renderQPostForm = (formEnabled) => {
      console.log(formEnabled);
      if(formEnabled){
          return <QResponsePostForm></QResponsePostForm>;
      }
  }

  renderQListItems = (qListItems) => {
      if(qListItems != null){
            return qListItems.map(qItem => (
                <QItem key={qItem.id} id={qItem.id} qThreadId={ qItem.id } qItemClickHandler={ this.loadQItem } text={qItem.text}/>
            ))
      }
  }

  render() {
    const { appLoaded, qThreadReady, cList, currentPage, qListItems, previous, next, error } = this.props;
    let body = <div></div>
    if (!appLoaded) {
        let bodyStyle = {color: "white"}
        body = <div style={bodyStyle}>App Loading ...</div>
    }
    else{
        if(error){
            body = <div>OPS! Something went wrong!</div>
        }
        else{
            body = <React.Fragment>
                <div className="left-container">
                <ul className="left-menu">
                    {this.renderQListItems(qListItems)}
                </ul>
                <Pagination currentPage={ currentPage } hasPrev={ previous } hasNext={ next }></Pagination>
            </div>
            <div className="right-container" id="id_chat_detail">
                { this.renderConversationBody(cList) }
            </div>      
            <div className="qresponse-post-form-container">
                { this.renderQPostForm(qThreadReady) }
            </div>      
            </React.Fragment>
        }        
    }  
    return (
        <React.Fragment>
            <Header className="App-heading" heading="Ariana Questionairre"/>
            <div id="main-container">
                {body}
            </div>
        </React.Fragment>
      );  
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatbotWrapper);
