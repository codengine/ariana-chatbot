import React, {Component} from "react"


class ChatRow extends Component{
    render(){
        return (
            <div className={this.props.className}><span>{this.props.from}</span> <span>{this.props.text}</span></div>
        )
    }
}

export default ChatRow;