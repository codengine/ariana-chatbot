import React, {Component} from "react"
import { connect } from 'react-redux';
import RestAPI from "../api/RestAPI"


const mapStateToProps = state => ({
    ...state.AppReducer,
    ...state.QListReducer
});

const mapDispatchToProps = dispatch => ({
    onPrevBtnClick: (qListItems, currentPage, hasPrev, hasNext, error) =>
        dispatch({  type: "PAGINATION_PREV_CLICK", qListItems, currentPage, hasPrev, hasNext, error }),
    onNextBtnClick: (qListItems, currentPage, hasPrev, hasNext, error) =>
        dispatch({  type: "PAGINATION_NEXT_CLICK", qListItems, currentPage, hasPrev, hasNext, error })
});

class Pagination extends Component{

    fetchQList = ( option, currentPage ) => {
        const { logger, previous } = this.props;

        let api = new RestAPI();
        let currentP = currentPage;
        if(!previous) {
            currentP = 1;
        }
        if(option === "PREV"){
            currentP -= 1;
        }
        else if(option === "NEXT"){
            currentP += 1;
        }

        logger.logInfo("Fetching QList with option: " + option + " and page: " + currentP);

        api.fetchQList(option, currentP).then(response=>{
            
            console.log(response);

            if(option === "PREV"){
                this.props.onPrevBtnClick(response.results, currentP - 1, response.previous, response.next, null);
            }
            else if(option === "NEXT"){
                this.props.onNextBtnClick(response.results, currentP, response.previous, response.next, null);
            }
        }).catch(error=>{
            console.log(error);
            if(option === "PREV"){
                this.props.onPrevBtnClick([], currentP - 1, false, false, error);
            }
            else if(option === "NEXT"){
                this.props.onNextBtnClick([], currentP, false, false, error);
            }
        })
    }

    render(){
        const { currentPage, previous, next } = this.props;
        let prevElement = <li></li>;
        let nextElement = <li></li>;
        if(previous){
            prevElement = <li className="left" onClick={e => {e.preventDefault(); this.fetchQList("PREV", currentPage)}}><a href="#">{"<< Prev"}</a></li>;
        }
        if(next){
            nextElement = <li className="right" onClick={e => {e.preventDefault(); this.fetchQList("NEXT", currentPage)}}><a href="#">{"Next >>"}</a></li>;
        }
        return (
            <ul className="pagination">
                { prevElement }
                { nextElement }
            </ul>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);