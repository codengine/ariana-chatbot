import React, {Component} from "react"


class Header extends Component{
    render(){
        return (
            <header>
                <h1 className={this.props.className}>{this.props.heading}</h1>
            </header>
        )
    }
}

export default Header;