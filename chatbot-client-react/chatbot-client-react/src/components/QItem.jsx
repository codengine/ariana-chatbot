import React, {Component} from "react"


class QItem extends Component{
    render(){
        const {id, text, qItemClickHandler, qThreadId, liClassName} = this.props;
        return (
            <li key={id} onClick={e => {qItemClickHandler(qThreadId)}} className={liClassName}>
            <a>{text}</a>
            </li>
        )
    }
}

export default QItem;