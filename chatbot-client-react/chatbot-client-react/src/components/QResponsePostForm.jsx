import React, {Component} from "react"
import { connect } from 'react-redux';
import RestAPI from "../api/RestAPI"

const mapStateToProps = state => ({
    ...state.QListReducer,
    logger: state.AppReducer.logger,
    storageInstance: state.AppReducer.storageInstance,
    clientId: state.AppReducer.clientId,
    qThreadId: state.QListReducer.qThreadId,
    conversationId: state.QThreadReducer.lastConversationQId,
    qEnd: state.QThreadReducer.qEnd
});

const mapDispatchToProps = dispatch => ({
    onSubmitQResponse: (messageFrom, qText, rowClassName, hasNext) =>
        dispatch({ type: "SUBMIT_QRESPONSE", messageFrom, qText, rowClassName, hasNext: hasNext }),
    onFetchNextQ: (id, messageFrom, qText, rowClassName, hasNext) => 
        dispatch({ type: "FETCH_NEXT_Q", id, messageFrom, qText, rowClassName, hasNext })
  });


class QResponsePostForm extends Component{

    constructor() {
        super();
        this.state = {
          response: ''
        };
    
        this.setResponse = ev => {
          this.setState({ response: ev.target.value });
        };
    
        this.submitResponse = ev => {
            const { logger } = this.props;
            let $this = this;
            ev.preventDefault();
            let api = new RestAPI();
            const { clientId, qThreadId, conversationId } = this.props;
            let answer = this.state.response;
            if(answer){
                api.submitQResponse(clientId, qThreadId, conversationId, answer).then(response=>{
                    console.log(response);
                    let messageFrom = "You: ";
                    let message = answer;
                    let rowClassName = "user-chat-row";
                    this.props.onSubmitQResponse(messageFrom, message, rowClassName);
                    return conversationId;

                }).then(conversationId => {
                    // Now fetch the next question
                    api.loadNextQ(conversationId, answer).then(response => {
                        let responseObject = response.object;
                        let hasNext = response.has_next;
                        if(responseObject != null){
                            logger.logInfo("Fetched the next question!");
                            logger.logInfo(response);

                            let nextId = responseObject.id;
                            let qText = responseObject.text;
                            let messageFrom = "Ariana: ";
                            let rowClassName = 'ariana-chat-row';
                            this.props.onFetchNextQ(nextId, messageFrom, qText, rowClassName, true);
                        }
                        if(!hasNext){
                            logger.logInfo("No next question found!");
                            let nextId = null;
                            let qText = "Thank you!";
                            let messageFrom = "Ariana: ";
                            let rowClassName = 'ariana-chat-row';
                            this.props.onFetchNextQ(nextId, messageFrom, qText, rowClassName, false);

                            // Now send the signal to the server to print the conversation.
                            api.sendResponseToServer(clientId, qThreadId).then(response => {
                                logger.logInfo("Signal sent to the server.");
                            }).catch(error => {
                                logger.logInfo("Signal sending failed.");
                            })
                        }
                        else{
                            if(responseObject == null){
                                if(response.message != null){
                                    let nextId = null;
                                    let qText = response.message;
                                    let messageFrom = "Ariana: ";
                                    let rowClassName = 'ariana-chat-row';
                                    this.props.onFetchNextQ(nextId, messageFrom, qText, rowClassName, true);
                                }
                                else {
                                    let nextId = null;
                                    let qText = "Something went wrong!";
                                    let messageFrom = "System: ";
                                    let rowClassName = 'system-chat-row';
                                    this.props.onFetchNextQ(nextId, messageFrom, qText, rowClassName, false);
                                }
                            }
                            
                        }
                    }).catch(error => {
                        let nextId = null;
                        let qText = "Something went wrong!";
                        let messageFrom = "System: ";
                        let rowClassName = 'system-chat-row';
                        this.props.onFetchNextQ(nextId, messageFrom, qText, rowClassName, false);
                    });
                }).catch(error=>{
                    //console.log(error);
                    let messageFrom = "You: ";
                    let qText = answer;
                    let rowClassName = "user-chat-row";
                    this.props.onSubmitQResponse(messageFrom, qText, rowClassName, true);

                    messageFrom = "Ariana: ";
                    qText = "Invalid Response. Please follow the instrcution and type the correct response";
                    rowClassName = "user-chat-row";
                    this.props.onSubmitQResponse(messageFrom, qText, rowClassName, true);
                });
            }
            else {
                // Text field is blank.
                return false;
            }
        };
      }

    render(){
        return (
            <form className="qresponse-post-form" onSubmit={this.submitResponse}>
                <input type="text" 
                value={this.state.response}
                onChange={this.setResponse}
                pleaseholder="Type your response ..."></input>
                <button>Submit</button>
            </form>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QResponsePostForm);