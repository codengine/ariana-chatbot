import { combineReducers } from 'redux'
import AppReducer from "./AppReducer"
import QListReducer from "./QListReducer"
import QThreadReducer from "./QThreadReducer"
import QResponsePostReducer from "./QResponsePostReducer"


export default combineReducers({
    AppReducer,
    QListReducer,
    QThreadReducer,
    QResponsePostReducer
})