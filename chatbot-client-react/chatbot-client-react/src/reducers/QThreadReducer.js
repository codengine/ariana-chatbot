const QThreadReducer = (state = {
    currentQThreadId: null,
    lastConversationQId: null,
    qEnd: false,
    qThreadReady: false,
    items: []
}, action) => {
    switch(action.type) {
        case "QITEM_CLICKED":
            console.log(action.cId);
            let rowObject = {
                id: action.cId,
                messageFrom: action.messageFrom,
                qText: action.qText,
                rowClassName: action.rowClassName
            }
            let items = [];
            items.push(rowObject)
            console.log(action.error);
            return {
                ...state,
                items: items,
                currentQThreadId: action.qThreadId,
                lastConversationQId: action.cId,
                qThreadReady: !action.error? true: false
            }
        case "SUBMIT_QRESPONSE":
        case "FETCH_NEXT_Q":
            rowObject = {
                messageFrom: action.messageFrom,
                qText: action.qText,
                rowClassName: action.rowClassName
            }
            items = [...state.items]
            items.push(rowObject)
            let lastCId = null;
            if(action.id){
                lastCId = action.id;
            }
            else if(state.lastConversationQId){
                lastCId = state.lastConversationQId;
            }
            console.log(lastCId);
            return {
                ...state,
                items: items,
                lastConversationQId: lastCId,
                qThreadReady: action.hasNext? true: false
            }
        default:
            return state;
    }
  }
  export default QThreadReducer;