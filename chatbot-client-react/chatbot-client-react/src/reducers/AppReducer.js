// import {APP_LOADED, APP_UNLOADED, QITEM_CLICKED} from "../actions/ActionTypes"


const AppReducer = (state = {}, action) => {
    switch(action.type) {
      case "APP_LOADED":
        return {
          ...state,
          appLoaded: true
        };
      case "ATTACH_LOGGER":
        return {
          ...state,
          logger: action.loggerInstance
        }
      case "ATTACH_LOCAL_STORAGE":
        return {
          ...state,
          storageInstance: action.storageInstance
        }
      case "ATTACH_CLIENT_ID":
        return {
           ...state,
           clientId: action.clientId
        }
      default:
        return state;
    }
  }
  export default AppReducer;