const QListReducer = (state = {
    qThreadId: null,
    cId: null,
    lastConversationQId: null,
    qListItems: [],
    previous: false,
    next: false,
    cList: [],
    error: null,
    message: null,
    currentPage: -1
}, action) => {
    switch(action.type) {
      case "APP_LOADED":
        return {
          ...state,
          count: action.count,
          qListItems: action.qListItems,
          previous: action.previous,
          next: action.next,
          error: action.errors,
          currentPage: 1,
          cList: []
        };
      case "QITEM_CLICKED":
          return {
            ...state,
            qThreadId: action.qThreadId,
            initialLoad: action.initialLoad,
            qText: action.qText, 
            cId: action.cId,
            qParentId: action.qParentId,
            error: action.error, 
            message: action.message
          };
      case "PAGINATION_PREV_CLICK":
          return {
            ...state,
            qListItems: action.qListItems,
            previous: action.hasPrev,
            next: action.hasNext,
            error: action.error,
            currentPage: action.currentPage,
          };
      case "PAGINATION_NEXT_CLICK":
          return {
            ...state,
            qListItems: action.qListItems,
            previous: action.hasPrev,
            next: action.hasNext,
            error: action.error,
            currentPage: action.currentPage,
          };
      default:
        return state;
    }
  }
  export default QListReducer;