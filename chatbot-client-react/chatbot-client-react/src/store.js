import { createStore, applyMiddleware } from "redux";
import reducer from "./reducers/index"

const initialState = {};

const middleware = [];

const store = createStore(reducer, initialState, applyMiddleware(...middleware));

export default store;