import React, { Component } from 'react';
import { Provider } from "react-redux";
import { connect } from 'react-redux';
import Header from "./components/Header"
import ChatbotWrapper from "./components/ChatbotWrapper"
import store from "./store"
// import {APP_LOADED, APP_UNLOADED, QITEM_CLICKED} from "./actions/ActionTypes"

import './Style.css';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App">
            <ChatbotWrapper></ChatbotWrapper>
        </div>
      </Provider>
    )    
  }
}

export default App;