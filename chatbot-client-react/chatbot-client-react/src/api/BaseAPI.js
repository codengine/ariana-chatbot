import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

class BaseAPI{

    constructor(){
        this.setBaseURL();
        this.requestHeaders = {
            'X-Requested-With' : "XMLHttpRequest"
          }
    }

    setBaseURL = (baseURL = "http://127.0.0.1:8001/api/1.1/") => {
        this.baseURL = baseURL;
    }

    callAPI = (url, method="get", payload={}) => {
        let apiUrl = this.baseURL + url;
        if(method === "get"){
            let promise = new Promise((resolve, reject) => {
                // fetch(apiUrl, {
                //     headers: {
                //     'X-Requested-With': "XMLHttpRequest"
                //     }
                // })
                // .then(res => res.json())
                // .then(
                // (response) => {
                //     resolve(response);
                // },
                // (error) => {
                //     reject(error);
                // }
                // )
                axios.get(apiUrl).then((res) => resolve(res.data)).catch(reject);
            });
            return promise;
        }
        else{
            let promise = new Promise((resolve, reject) => {
                // fetch(apiUrl, {
                //     method: "POST",
                //     body: JSON.stringify(payload),
                //     headers: {
                //         'X-Requested-With': "XMLHttpRequest",
                //         'Accept': 'application/json',
                //         'Content-Type': 'application/json'
                //     }
                // })
                // .then(res => res.json())
                // .then(
                // (response) => {
                //     resolve(response);
                // },
                // (error) => {
                //     reject(error);
                // }
                // )
                console.log(payload);
                axios.post(apiUrl, payload).then((res) => resolve(res)).catch(reject);
            });
            return promise;
        }        
    }

}

export default BaseAPI;
