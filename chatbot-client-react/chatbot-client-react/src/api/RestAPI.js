import BaseAPI from "./BaseAPI"


class RestAPI extends BaseAPI{
    fetchQList = (option, currentPage=1) => {
        let url = "conversation-list/";
        url += "?page=" + currentPage;
        return this.callAPI(url);
    }

    sendResponseToServer = (clientId, qThreadId) => {
        let url = "conversation-end/";
        let payload = {
            "client_id": clientId,
            "base_question_id": qThreadId
        }
        return this.callAPI(url, "post", payload);
    }

    submitQResponse = (clientId, qThreadId, conversationId, answer, qEnd=false) => {
        if(qEnd){
            return false;
        }
        let payload = {
            "client_id": clientId,
            "first_question_id": qThreadId,
            "conversation_id": conversationId,
            "answer": answer
        };
        let url = "save-response/1/";
        return this.callAPI(url, "post", payload);
    }

    loadNextQ = (qThreadId, text) => {
        let url = "conversation/" + qThreadId + "/";
        if(text != null){
            url += "?text=" + text;
        }
        return this.callAPI(url);
    }
}

export default RestAPI;