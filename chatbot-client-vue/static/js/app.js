// Logger
var SafeLogger = (function () {
  var is_debug_enabled = true;
  var default_font_style_size = "15px";
  var default_log_style_info = "font-size: 13px; color: green; display: block; text-align: center;";
  var default_log_style_warn = "font-size: 13px; color: yello;";
  var default_log_style_error = "font-size: 13px; color: red;";
  var style_enabled = true;
  var SafeLogger = function(isDebug, disable_style) {
    is_debug_enabled = isDebug;
	style_enabled = !disable_style;
	if(window.console && is_debug_enabled){
		console.log("%cWarning! DEBUG ENABLED", "color: red; font-size: 30px;");
	}
	else{
		console.log("%cWarning! Production. Please do not change anything.", "color: red; font-size: 30px; display: block;");
		window.console = null;
	}
  }
  SafeLogger.prototype = {
    log_info: function (data) {
        if (window.console && is_debug_enabled) {
			if(style_enabled){
				console.log("%c" + data, default_log_style_info);
			}
			else{
				console.log(data);
			}
        }
    },
	log_info_group: function(group_title, items, collapsed){
		var $this = this;
		if (window.console && is_debug_enabled) {
			if(collapsed){
				console.groupCollapsed("%c" + group_title, "font-size: " + default_font_style_size);
			}
			else{
				console.group("%c" + group_title, "font-size: " + default_font_style_size);
			}
			for(var i = 0; i < items.length; i++){
				$this.log_info(items[i]);
			}
			console.groupEnd();
        }
	},
	log_warning: function(data){
		if (window.console && is_debug_enabled) {
			if(style_enabled){
				console.warn("%c" + data, default_log_style_warn);
			}
			else{
				console.warn(data);
			}
        }
	},
	log_warning_group: function(group_title, items, collapsed){
		var $this = this;
		if (window.console && is_debug_enabled) {
			if(collapsed){
				console.groupCollapsed("%c" + group_title, "font-size: " + default_font_style_size);
			}
			else{
				console.group("%c" + group_title, "font-size: " + default_font_style_size);
			}
			for(var i = 0; i < items.length; i++){
				$this.log_warning(items[i]);
			}
			console.groupEnd();
        }
	},
	log_error: function(data){
		if (window.console && is_debug_enabled) {
            if(style_enabled){
				console.error("%c" + data, default_log_style_error);
			}
			else{
				console.error(data);
			}
        }
	},
	log_error_group: function(group_title, items, collapsed){
		var $this = this;
		if (window.console && is_debug_enabled) {
			if(collapsed){
				console.groupCollapsed("%c" + group_title, "font-size: " + default_font_style_size);
			}
			else{
				console.group("%c" + group_title, "font-size: " + default_font_style_size);
			}
			for(var i = 0; i < items.length; i++){
				$this.log_error(items[i]);
			}
			console.groupEnd();
        }
	},
  };
return SafeLogger;
})();

// Storage Class
var SafeLocalStorage = (function () {
    function is_local_storage_enabled(){
        if (typeof localStorage !== 'undefined') {
        try {
                localStorage.setItem('feature_test', 'yes');
                if (localStorage.getItem('feature_test') === 'yes') {
                    localStorage.removeItem('feature_test');
                    // localStorage is enabled
                    return true;
                } else {
                    // localStorage is disabled
                    return false;
                }
            } catch(e) {
                // localStorage is disabled
                return false;
            }
        } else {
            // localStorage is not available
            return false;
        }
    }
    var storage_available = is_local_storage_enabled();
    var storage_key = "ariana-v1.1";
    var SafeLocalStorage = function () {

    }

    SafeLocalStorage.prototype = {
        is_storage_available: function () {
            return storage_available;
        },
        get_storage_key: function () {
            return storage_key;
        },
        get_client_id: function() {
            if(storage_available){
                var client_id = "ariana_client_id";
                var c_id = localStorage.getItem(client_id);
                if(c_id == null){
                    var random_id = Math.random().toString(36).substring(2);
                    try{
                        localStorage.setItem(client_id, random_id);
                        return localStorage.getItem(client_id);
                    }catch(e){
                        return null;
                    }
                }
                else{
                    return localStorage.getItem(client_id);
                }
            }
            return null;
        },
        save_local: function(key, data){
            var save_locally = new Promise(function(resolve, reject) {
                if(storage_available){
                    try {
                        // Read the existing object
                        var chat_dialogs = localStorage.getItem(storage_key);
                        if(chat_dialogs === null){
                            chat_dialogs = {};
                        }
                        else{

                        }
                        chat_dialogs[key] = data;
                        localStorage.setItem(storage_key, chat_dialogs);
                        resolve("Saved");
                    } catch(e) {
                        reject(e);
                    }
                }
                else{
                    reject("STORAGE_NOT_AVAILABLE");
                }
            });
            return save_locally;
        },
        read_storage: function(key){
            var promise = new Promise(function(resolve, reject){
                if(storage_available){
                    var chat_dialogs = localStorage.getItem(storage_key);

                    if(chat_dialogs != null){
                        console.log("-1 " + chat_dialogs);
                        var item = chat_dialogs[key];
                        resolve(item);
                    }
                    else{
                        resolve(null);
                    }
                }
            });
            return promise;
        }
    }
    return SafeLocalStorage
})();

String.prototype.capitalize = function () {
    return this.toLowerCase().split(' ').map(function (i) {
        if (i.length > 1) {
            return i.charAt(0).toUpperCase() + i.substr(1);
        } else {
            return i;
        }
    }).join(' ');
};

// Vue components
Vue.component('loading-screen', {
  template: '<div id="loading">Loading...</div>'
});

Vue.component('form_input', {
    template: '#id_response_form',
    props : ['form_element'],
    data : function(){
        return {

        }
    }
});

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
var logger = new SafeLogger(true, false);
var storage_handler = new SafeLocalStorage();

var app = new Vue ({
    el: '#id_app',
    data: {
        is_loading: true,
        show_modal: false,
        board_loaded: false,
        page_title: "Ariana - The health Assistant",
        page_heading: "Ariana - Questionaire",
        pageHeading: "page-heading",
        api_url_domain: "http://127.0.0.1:8001/api/1.1/",
        content_area: "list", //create or list
        loading: true,
        form_data: {},
        post_url: "",
        q_list: [],
        q_started: false,
        questions: [],
        current_q_thread: -1,
        last_q_item: null,
        user_response: "",
        q_text : "",
        user_response_list: [],
        q_end: false,
        q_list_prev: false,
        q_list_next: false,
        q_list_current_page: 0
    },
    methods:{
        call_ajax: function(url, method, data, success_callback, error_callback) {
            var log_data = ["URL" + url, "Request method" + method, "Data: " + data];
            logger.log_info_group("Inside submit ajax method", log_data);
            if(method == "GET") {
                axios.get(url, {
                }).then(success_callback, error_callback);
            }
            else if(method == "POST"){
                axios.post(url, data).then(success_callback).catch(error_callback);
            }
        },
        submit_ajax: function (url, method, data) {
            var promise = new Promise(function (resolve, reject) {
                if(method == "GET") {
                    axios.get(url).then(resolve).catch(reject);
                }
                else if(method == "POST"){
                    axios.post(url, data).then(resolve).catch(reject);
                }
            });
            var log_data = ["URL" + url, "Request method" + method, "Data: " + data];
            logger.log_info_group("Inside submit ajax method", log_data);
            return promise;
        },
        send_response_to_server: function () {
            logger.log_info("Conversation ended. Sending signal to the server for console print");
            var $this = this;
            var url = this.api_url_domain + "conversation-end/";
            var data = {
                "client_id": storage_handler.get_client_id(),
                "base_question_id": $this.current_q_thread
            }
            $this.submit_ajax(url, "POST", data).then(function(response){
                $this.q_end = true;
                logger.log_info("Response sent." + response);
            }).catch(function(error){
                logger.log_error("Response send failed " + error);
            });
        },
        load_next_converstion: function(q_id, text, callback){
            logger.log_info("Inside load next conversation");
            var $this = this;
            var url = this.api_url_domain + "conversation/"+q_id+"/";
            var data = {};
            if(typeof text != "undefined"){
                url += "?text=" + text;
            }
            else{
                url += "?text=";
            }
            
            var clnt_id = storage_handler.get_client_id();
            if(clnt_id != null){
                url += "&client_id=" + clnt_id;
            }

            $this.submit_ajax(url, "GET", {}).then(function(response){
                var response_data = response.data;
                var is_last_item = !response_data.has_next; //response.data.last_item;
                var item = response_data.object;
                if(typeof item != "undefined" && item != null && Object.keys(item).length === 0 && item.constructor === Object){
                    if(!is_last_item){
                        var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;" data-qid="'+ item.id +'">Araina: Invalid response</p>';
                         $this.questions.push(html);
                        if(typeof callback == "function"){
                            callback(false);
                        }
                    }

                }
                else{
                    $this.last_q_item = item.id;
                    var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;" data-qid="'+ item.id +'">Araina: '+ item.text +'</p>';
                    $this.questions.push(html);
                    if(typeof callback == "function"){
                        callback(item);
                    }
                }

            }).catch(function(error){
                callback(false);
            });
            return false;
        },
        get_all_questionnaire: function(option){
            this.q_list = [];
            var $this = this;
            $this.questions = [];
            $this.board_loaded = false;
            var url = this.api_url_domain + "conversation-list/";
            if(typeof option != "undefined"){
                if(option == "prev"){
                    $this.q_list_current_page -= 1;
                    url += "?page=" + $this.q_list_current_page;
                    $this.q_list_current_page -= 1;
                }
                else if(option == "next"){
                    url += "?page=" + ($this.q_list_current_page + 1);
                }
            }
            this.call_ajax(url, "GET", {}, function(response){
                var data = response.data;
                var count = data.count;
                if(data.previous != null){
                    $this.q_list_prev = true;
                }
                else{
                    $this.q_list_prev = false;
                }
                if(data.next != null){
                    $this.q_list_next = true;
                }
                else{
                    $this.q_list_next = false;
                }
                if(count > 0){
                    $this.q_list_current_page += 1;
                    $this.q_list = data.results;
                }
                else{
                    $this.q_list = [];
                }

            }, function(error){

            });
            return false;
        },
        handle_questionnaire_click: function (q_id) {
            logger.log_info("Clicked on questionnaire item with id: " + q_id);
            var $this = this;
            $this.questions = [];
            $this.form_data = {};
            $this.current_q_thread = q_id;
            $this.last_q_item = null;
            $this.board_loaded = true;
            if($this.q_started){
                $this.q_started = false;
            }
            if(!$this.q_started){
                logger.log_info("Loading questionnaire");
                $this.load_next_converstion(q_id, "", function(item){
                    $this.q_text = item.text;
                });
                $this.q_started = true;
                $this.q_end = false;
            }

        },
        fetch_next: function () {
            logger.log_info("Inside fetch next");
            var $this = this;
            url = $this.api_url_domain + "conversation/" + $this.last_q_item + "/";
            if($this.form_data.response){
                url += "?text=" + $this.form_data.response;
            }
            $this.submit_ajax(url, "GET").then(function (response) {
                logger.log_info(response.status);
                if(response.status == 200){
                    var data = response.data;
                    var data_object = data.object;
                    if(data_object != null){
                        $this.last_q_item = data_object.id;
                        var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;" data-qid="'+ data_object.id +'">Ariana: '+ data_object.text +'</p>';
                        $this.questions.push(html);
                    }

                    if(data.has_next == false){
                        var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;">Ariana: Thank you!</p>';
                        $this.questions.push(html);
                        $this.board_loaded = false;

                        // Now print the response in server console
                        $this.send_response_to_server();
                    }
                    else{
                        if(data_object == null){
                            if(data.message != null){
                                var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;">Ariana: '+ data.message +'</p>';
                                $this.questions.push(html);
                            }
                            else{
                                var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;">Ariana: Sorry! Something went wrong.</p>';
                                $this.questions.push(html);
                            }
                        }
                        $this.board_loaded = true;
                    }
                    $this.form_data = {};
                }
                else{
                    return false;
                }
            }).catch(function (err) {
                var html = '<p style="display: inline; background-color: #79aec8; border-radius: 10px; padding: 5px; color: white;">Ariana: OOPS! Something went wrong.</p>';
                $this.questions.push(html);
                $this.board_loaded = false;
            });
        },
        submit_q_form: function() {
            logger.log_info("Inside submit form data");
            var $this = this;
            if($this.q_end){
                logger.log_info("Conversation end");
                return false;
            }
            if($this.form_data.response && $this.form_data.response.trim()){
                var last_q_item = $this.last_q_item;

                var html = '<p style="display: inline; background-color: #8c939d; border-radius: 10px; padding: 5px; color: white;">You: '+ $this.form_data.response +'</p>';
                $this.questions.push(html);
                // Submit the data
                var data = {
                    "client_id": storage_handler.get_client_id(),
                    "first_question_id": $this.current_q_thread,
                    "conversation_id": last_q_item,
                    "answer": $this.form_data.response
                };

                var url = $this.api_url_domain + "save-response/1/";
                $this.submit_ajax(url, "POST", data).then(function (response) {
                    if(response.status == 201){
                        return true;
                    }
                    else{
                        return false;
                    }
                }).then(function (success) {
                    $this.fetch_next();
                }).catch(function (err) {
                    $this.fetch_next();
                });
            }
            else{
                return false;
            }
        }
    },
    mounted: function () {
        var $this = this;
        this.get_all_questionnaire();
        setTimeout(function () {
            $this.is_loading = false
        }, 2000);
    }
});
