# Ariana - THE Chatbot

This chatbot is made based on the specification provided.

# Overview

This chatbot system (Ariana) is a basic questionnare system. User can choose a questionnaire they wish to continue with. So basically in the questionnaire
page they see a list of questionnaire. They can choose anyone and interact accordingly. When the last response is submitted, the backend API print the 
whole conversation with format: 

first_question_text: answer1 -> answer2 -> last_answer

Every limitations mentioned in the task details has been obeyed.

The whole project has two parts: Front-end and Back-end

# The Backend

The backend system is built on top of Python/DJango/DJangoRestFramework. I have used Python3.6 and SQLite as database with ORM. So the database can be 
just changed anytime with any relational DB supported by django. For non-rel, I would need to either write a new backend or port an existing backend with
the project.

The API is focused on the following implementation:

1. APIs -> I have implemented the correct status code in API response. For example, post always return 201 on success, get success always return 200, 
Internal server error always return 500, Method not allowed always return 405, Bad request always return 400, Invalid request policy(like normal request instead of ajax)
where ajax only is enabled in the backend returns always 401 and so on

2. I have seperated the core logic in core app. So core features which will be reused throughout the whole projects by any app have been implemented in the core.
For example, I have written a base_entity abstract model which must be used by all other app so all common fields are put in the base_entity model and 
must be reused by the other apps. Similarly for other parts.

3. I have configured the logging system to log development and production in console/files seperately based on the DEBUG settings.

4. I have implemented the CORS using a third party APP. So CORS can be enabled and disabled. If CORS enabled then cross ajax can be made from the client app otherwise 
no ajax call can be made.

5. I have implemented ajax only feature, meaning the API's can be restricted to be used only using ajax call.

6. I have tried to follow the DRY principle with my best. If you look at the project structure then it could be better understood.

7. Questionnaires are loaded from the json file located in the data directory. So now it allows only 5 option at max(as per the limitation in spec). So
what it does, if it detects options more than 5, it just skips the extra options and log a message. So it could be just implemented in other way like that,
if it finds more than 5 options then it would skip the enity question tree from there.

8. I have tried to show my ability to build REST API's in a proper way

9. There are no authentication/authorization feature. To refer a client indepedantly what I did, I generate a client id in the client app and send this with 
the request. The client app stores the client_id in local storage which are reused subsequently.

10. If authentication is implemented then I would use Token based authentication to perform all API communication after the token is obtained. The token would be
placed in header of every request. I would not store the token in localStorage. Instead I would use indexedDB.

11. I was thinking to implement the cross tabs sync using localStorage. I did not implement the Storage listener but it could be implemented very easily.

12. If a user start a questionnaire and switch to other before finishing it, it doesn't resume when he gets back to the previous one. Also the backend 
is not aware whether the user has left the questionnaire unfinished or not. This is one case I just noticed. So it will not print the correct message in the console
at the final respnse.
For example, q1 user hass responded Yes->Sure then switched to the q2. q1 is unfinished. Now he gets back to the q1 and q1 started from scratch. He answered
Yes->No->Bye
The backend will print Yes->Sure->Yes->No->Bye
This issue I just noticed. So This can be easily fixed by cleaning the previous q1 response when he switched to a new questions.

	Update: I have fixed it in bitbucket online. I will check after my office. Although it should be fixed just for your reference.

13. No docker implementation provided. I just need some time to write the docker file and docker compose file for this full stack app.
I would use gunicorn/nginx for Python/DJango and nginx for the static client app. Should I do it?

14. I have tried you to understand my ability to build a robust full stack web application with any features.

15. Obviously if more time is given this app would be more robust and there might be countless features/capabilities we could bring in to the app.

16. API test cases have been implemented. I could write TDD but it would have taken double time. So better I have shown you my ability to write unit testing
which I do in my projects.

17. Implemented a chat model which keeps track of the entire successful response from the user for a particular questionnaire list.

18. Implemented few decorators

19. I have exempted the csrf token feature which is not the correct way. If I did not do it I have to correct the csrf token in the client side from cookie and
send it with every post request. I am already exhausted and that's why I skipped using the csrf token feature. Please let me know if you really need me to do it.


# Front-end:
The front-end is built on top of Vue JS. For ajax call, I have used axios library. The front-end is focused on the following features:

1. The vue dual binding capability has been extensively used which has ease the development/management in many cases.

2. Implemented a SafeLogger by overriding the default console.log. The reason is we can add anything throughout the app anytime we would need. We just need to
make changes our SafeLogger class methods.

3. The logger obeyes the production/development environment. In both cases it shows a warning message but in production it removes the window.console so
no one could use the window.console in production environment. The logger is capable to be turned on/off and the log messages are custom styled/groupped.

4. The environment mode can be turned on/off when instatiating the SafeLogger. The first parameter in the constructor takes care of this whereas the second
parameter takes care if default style need to be used forceably. By default it uses overridden style for console.

5. The localStorage feature. When it save the user response, it tries to find if there any client id already generated for this client app. If so it returns 
otherwise it creates, saves to localStorage and returns. This client id being referred subsequently.

6. If the backend api is restricted to use Ajax only then the axios global header need to be modified to send the request as XMLHttpRequest.

7. The backend is capable to handle preflight requests now so using ajax calls now run successfully which was an issue earlier.

8. The ajax call is implemented using traditional callback and also using promise, just to show my knowledge and ability of doing that. Please don't 
think the implementation of the same method in two different ways as ugly code because I did it intentionally and part of my methods use the first one 
and the others use the second one.

9. The questionnaire list is paginated and shows previous and next button. The size of pagination is controlled by the backend core->api->pagination->ariana_pagination

10. After the last question is answered, the chatbot says Thank you notifying the user that the conversation has ended and the input form is
disabled.

11. Added loading features and to feel the loading I made setTimeout call in 2000ms.

 #Update:
I have build the frontend using react js as well.I have used redux for state management for the entire app.


# How to run the API: chatbot

1. Install python3.5+
2. Clone the project and cd to it
3. install virtualenv
4. activate it
5. pip install -r requirements.txt
6. Run makemigrations and migrate # For fresh run. Then run python manage.py load_q
7. If questionnaires need to be changed, change it in the data directory and run python manage.py load_q
8. Now run python manage.py runserver 8001

# How t run the vue client
1. Open the app.js
2. Change the app_url_domain to:
scheme://domain_name[:port]/api/version/
for example, http://127.0.0.1:8001/api/1.l/
3. Now load the html page in browser.
4. I have done all the development and testing in chrome. For other browser I did not check anything. 
If it's a requirement also I will check for other browser.But as I have a day job here I tried to show the 
capability that is expected from a full stack developer.

# How to run the React Client
1. Install node 10+
2. cd to the ariana-client-react
3. npm install
4. npm start
5. Browse http://127.0.0.1:3000/

