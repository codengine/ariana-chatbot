from django.test import TestCase
import os
import json
from unittest.mock import Mock, patch
from core.arianaio.file_manager import FileManager


class TestFileManager(TestCase):

    def setUp(self):
        self.file = os.path.join(os.path.dirname(__file__), 'data', 'q.json')

    def test_file_read(self):
        json_data = FileManager.read_json(self.file)
        self.assertEqual(len(json_data), 4)

    def tearDown(self):
        pass


class TestFileManagerError(TestCase):
    def setUp(self):
        self.file = "some_invalid_path"

    def test_file_read_exception(self):
        json_data = FileManager.read_json(self.file)
        self.assertEqual(len(json_data), 0)

    def tearDown(self):
        pass
