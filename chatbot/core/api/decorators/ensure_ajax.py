import logging
from functools import wraps
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings


logger = logging.getLogger("core")


def ensure_ajax(func):
    @wraps(func)
    def wrapped(request, **kwargs):
        if settings.ALLOW_AJAX_ONLY:
            if not request.is_ajax():
                logger.warn("Request must be using ajax.")
                return Response({'message': 'Request must be using Ajax'}, status=status.HTTP_401_UNAUTHORIZED)
        return func(request, **kwargs)
    return wrapped
