import logging
from functools import wraps
from rest_framework.response import Response
from rest_framework import status


logger = logging.getLogger(__file__)


def wrap_exceptions(func):
    @wraps(func)
    def wrapped(request, **kwargs):
        try:
            response = func(request, **kwargs)
            return response
        except Exception as exp:
            logger.debug("Exception occurec inside method: %s. Message: %s" % (str(func), str(exp)))
            return Response({'message': 'Internal Server Error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return wrapped