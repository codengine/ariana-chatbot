from rest_framework.views import APIView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.api.decorators.wrap_exceptions import wrap_exceptions
from core.api.decorators.ensure_ajax import ensure_ajax


class BaseAPIView(APIView):
    @method_decorator(ensure_ajax)
    @method_decorator(wrap_exceptions)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BaseAPIView, self).dispatch(request, *args, **kwargs)