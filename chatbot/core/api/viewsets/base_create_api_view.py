from rest_framework import generics
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from core.api.decorators.wrap_exceptions import wrap_exceptions
from core.api.decorators.ensure_ajax import ensure_ajax


class BaseCreateAPIView(generics.CreateAPIView):
    @method_decorator(ensure_ajax)
    @method_decorator(wrap_exceptions)
    @method_decorator(csrf_exempt)
    def post(self, request, **kwargs):
        return super(BaseCreateAPIView, self).post(request, **kwargs)
