from rest_framework import generics
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.api.decorators.wrap_exceptions import wrap_exceptions
from core.api.decorators.ensure_ajax import ensure_ajax


class BaseListAPIView(generics.ListAPIView):
    http_method_names = ["get"]

    @method_decorator(ensure_ajax)
    @method_decorator(wrap_exceptions)
    @method_decorator(csrf_exempt)
    def get(self, request, **kwargs):
        return super(BaseListAPIView, self).get(request, **kwargs)