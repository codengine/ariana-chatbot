from rest_framework import generics
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from core.api.decorators.wrap_exceptions import wrap_exceptions
from core.api.decorators.ensure_ajax import ensure_ajax


class BaseRetrieveAPIView(generics.RetrieveAPIView):
    @method_decorator(ensure_ajax)
    @method_decorator(wrap_exceptions)
    @method_decorator(csrf_exempt)
    def get(self, request, *args, **kwargs):
        return super(BaseRetrieveAPIView, self).get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
