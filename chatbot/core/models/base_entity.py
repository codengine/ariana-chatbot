from datetime import datetime
import pytz
from django.contrib.auth.models import User
from django.db import models


class BaseEntity(models.Model):
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(User, related_name="+", null=True, on_delete=models.CASCADE)
    updated_by = models.ForeignKey(User, related_name="+", null=True, on_delete=models.CASCADE)

    class Meta:
        abstract = True
        ordering = ["id"]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk:
            dt = datetime.utcnow()
            dt = dt.replace(tzinfo=pytz.utc)
            self.date_created = dt

        self.last_updated = datetime.utcnow()
        super(BaseEntity, self).save(force_insert, force_update, using, update_fields)