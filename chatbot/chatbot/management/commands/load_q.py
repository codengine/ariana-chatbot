import os
from django.core.management.base import BaseCommand
from core.arianaio.file_manager import FileManager
from chatbot.models.conversation import Conversation


class Command(BaseCommand):

   def handle(self, *args, **options):
        q_json_file = os.path.join(os.getcwd(), "data", "q.json")
        json_data = FileManager.read_json(q_json_file)
        Conversation.load_from_json(json_data=json_data)
