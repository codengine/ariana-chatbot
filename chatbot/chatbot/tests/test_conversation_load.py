from django.test import TestCase
import os
import json
from unittest.mock import Mock, patch
from core.arianaio.file_manager import FileManager
from chatbot.models.conversation import Conversation


class TestConversationLoad(TestCase):

    def setUp(self):
        self.file = os.path.join(os.path.dirname(__file__), 'data', 'q.json')

    def test_conversation_load(self):
        json_data = FileManager.read_json(self.file)
        self.assertEqual(Conversation.objects.all().count(), 0)
        Conversation.load_from_json(json_data)
        self.assertEqual(Conversation.objects.filter(parent__isnull=True).count(), 4)
        self.assertEqual(Conversation.objects.all().count(), 36)

        self.file = os.path.join(os.path.dirname(__file__), 'data', 'q_1.json')
        json_data = FileManager.read_json(self.file)
        Conversation.load_from_json(json_data)
        self.assertEqual(Conversation.objects.filter(parent__isnull=True).count(), 5)
        self.assertEqual(Conversation.objects.all().count(), 47)

    def tearDown(self):
        pass

