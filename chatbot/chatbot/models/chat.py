import logging
from django.db import models
from core.models.base_entity import BaseEntity
from chatbot.models.conversation import Conversation

logger = logging.getLogger("chatbot")


class Chat(BaseEntity):
    client_id = models.CharField(max_length=100)
    first_question = models.ForeignKey(Conversation, related_name="+", on_delete=models.CASCADE)
    conversation = models.ForeignKey(Conversation, related_name="+", on_delete=models.CASCADE)
    answer = models.CharField(max_length=200)
    valid_response = models.BooleanField(default=False)
    previous = models.ForeignKey(Conversation, null=True, blank=True, related_name="+", on_delete=models.CASCADE)
    next = models.ForeignKey(Conversation, null=True, blank=True, related_name="+", on_delete=models.CASCADE)
    status = models.CharField(max_length=50, null=True, blank=True) # NULL/COMPLETED
