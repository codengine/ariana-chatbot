import logging
from django.db import models
from core.models.base_entity import BaseEntity

logger = logging.getLogger("chatbot")


class Conversation(BaseEntity):
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)
    text = models.CharField(max_length=1000) # What would you like to eat? (Hamburger/Pizza/Pop Corn/Chicken) or Bot Response

    def has_next(self):
        return Conversation.objects.filter(parent_id=self.pk).exists()

    @classmethod
    def add(cls, text, parent=None):
        logger.info("Adding instance")
        instance = cls()
        instance.text = text
        if parent:
            instance.parent = parent
        instance.save()
        return instance

    @classmethod
    def save_q(cls, json_data, parent=None):
        if type(json_data) is dict:
            i = 0
            for q_text, q_body in json_data.items():
                i += 1
                instance = cls.add(text=q_text, parent=parent)
                cls.save_q(json_data=q_body, parent=instance)
                if i == 5:
                    logger.info("Found more than 5 items. Skipping ...")
                    break
        else:
            instance = cls.add(text=json_data, parent=parent)


    @classmethod
    def load_from_json(cls, json_data):
        logger.info("Clearing all exiting questions")
        cls.objects.all().delete()
        logger.info("Loading questionnaire data from json")

        for q, item in json_data.items():
            cls.save_q(json_data={q: item})

        logger.info("Total %s questionnaire added" % Conversation.objects.filter(parent__isnull=True).count())
        logger.info("Total %s items added" % Conversation.objects.all().count())

