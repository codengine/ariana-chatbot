import logging
import sys


class MessageLogFormatter(logging.Formatter):
    def __init__(self, fmt=None, datefmt=None, style='%'):
        super(MessageLogFormatter, self).__init__(fmt='Output: %(message)s', datefmt=datefmt, style=style)


class ArianaLogMessageFormatter(object):

    def __init__(self, logger=None):
        if logger:
            if not isinstance(logger, logging.getLoggerClass()):
                logger = None
        self.logger = logger

    @classmethod
    def get_console_logger(cls, logger):
        handlers = logger.handlers
        console_handler = None
        for handler in handlers:
            if handler.name == "console":
                console_handler = handler
                break
        return console_handler

    def __enter__(self):
        self.logger = self.logger or logging.getLogger()
        self.console_handler = self.__class__.get_console_logger(self.logger)
        self.default_formatter = None
        self.formatter_changed = False
        if self.console_handler:
            self.default_formatter = self.console_handler.formatter
            self.console_handler.formatter = MessageLogFormatter()
            self.formatter_changed = True
        return self

    def print_message_to_console(self, message):
        self.logger.info(message)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.formatter_changed:
            self.console_handler.formatter = self.default_formatter
