from django.test import TestCase
import os
import json
from django.conf import settings
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from chatbot.api.viewsets.questionaire_list_api_view import ConversationListAPIView
from chatbot.api.viewsets.conversation_api_view import ConversationAPIView
from chatbot.api.viewsets.response_create_api_view import ResponseCreateAPIView
from chatbot.api.viewsets.conversation_end_api_view import ConversationEndAPIView
from core.arianaio.file_manager import FileManager
from chatbot.models.conversation import Conversation


def setup(self):
    self.api_request_factory = APIRequestFactory()
    self.client = APIClient()
    self.host = 'http://127.0.0.1:8001/api/1.1'
    self.file = os.path.join(os.path.dirname(__file__), 'data', 'q.json')
    json_data = FileManager.read_json(self.file)
    Conversation.load_from_json(json_data)
    settings.ALLOW_AJAX_ONLY = False


class TestRestAPI(TestCase):

    def setUp(self):
        setup(self)

    def test_questionnaire_list_success(self):
        request = self.api_request_factory.get('/conversation-list/', content_type='application/json')
        view = ConversationListAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)

    def test_retrieve_conversation_api_success(self):
        request = self.api_request_factory.get('/conversation/1/', content_type='application/json')
        view = ConversationAPIView.as_view()
        response = view(request, **{'pk': 1})
        self.assertEqual(response.status_code, 200)

    def test_save_response_success(self):
        request = self.api_request_factory.post('/save-response/50/',
                                                json.dumps({
                                                "client_id": "12345",
                                                "first_question_id": 1,
                                                "conversation_id": 1,
                                                "answer": "Yes"
                                            }),content_type='application/json')
        view = ResponseCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 201)

    def test_conversation_end_api_view_success(self):
        request = self.api_request_factory.post('/conversation-end/', json.dumps({
                "client_id": "1234",
                "base_question_id": 1
            }),content_type='application/json')
        view = ConversationEndAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        settings.ALLOW_AJAX_ONLY = True


class TestRestAPIErrors(TestCase):

    def setUp(self):
        setup(self)

    def test_questionnaire_list_page_error(self):
        request = self.api_request_factory.get('/conversation-list/?page=-1', content_type='application/json')
        view = ConversationListAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 500)

    def test_questionnaire_list_405(self):
        request = self.api_request_factory.post('/conversation-list/', content_type='application/json')
        view = ConversationListAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 405)

    def test_save_response_405(self):
        request = self.api_request_factory.get('/save-response/50/', content_type='application/json')
        view = ResponseCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 405)

    def test_retrieve_conversation_api_405(self):
        request = self.api_request_factory.post('/conversation/1/', content_type='application/json')
        view = ConversationAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 405)

    def test_conversation_end_api_view_405(self):
        request = self.api_request_factory.get('/conversation-end/', {'text': 'text 1'},
                                                content_type='application/json')
        view = ConversationEndAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 405)

    def tearDown(self):
        settings.ALLOW_AJAX_ONLY = True


class APITestAllowAjaxOnly(TestCase):
    def setUp(self):
        setup(self)
        settings.ALLOW_AJAX_ONLY = True

    def test_questionnaire_list_page_not_allowed(self):
        request = self.api_request_factory.get('/conversation-list/', content_type='application/json')
        view = ConversationListAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.data, {'message': 'Request must be using Ajax'})

    def test_save_response_not_allowed(self):
        request = self.api_request_factory.post('/save-response/50/',
                                                json.dumps({
                                                    "client_id": "12345",
                                                    "first_question_id": 1,
                                                    "conversation_id": 1,
                                                    "answer": "Yes"
                                                }), content_type='application/json')
        view = ResponseCreateAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.data, {'message': 'Request must be using Ajax'})

    def test_retrieve_conversation_not_allowed(self):
        request = self.api_request_factory.get('/conversation/1/', content_type='application/json')
        view = ConversationAPIView.as_view()
        response = view(request, **{'pk': 1})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.data, {'message': 'Request must be using Ajax'})

    def test_conversation_end_api_view_not_allowed(self):
        request = self.api_request_factory.post('/conversation-end/', json.dumps({
            "client_id": "1234",
            "base_question_id": 1
        }), content_type='application/json')
        view = ConversationEndAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.data, {'message': 'Request must be using Ajax'})

    def tearDown(self):
        settings.ALLOW_AJAX_ONLY = True
