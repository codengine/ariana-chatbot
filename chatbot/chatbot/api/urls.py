from django.conf.urls import url
from chatbot.api.viewsets.questionaire_list_api_view import ConversationListAPIView
from chatbot.api.viewsets.conversation_api_view import ConversationAPIView
from chatbot.api.viewsets.conversation_end_api_view import ConversationEndAPIView
from chatbot.api.viewsets.response_create_api_view import ResponseCreateAPIView

urlpatterns = [
    url(r'^conversation-list/$', ConversationListAPIView.as_view(), name="conversation_list_api_view"),
    url(r'^conversation/(?P<pk>[0-9]+)/$', ConversationAPIView.as_view(), name="conversation_api_view_parent_id_get"),
    url(r'^save-response/(?P<pk>[0-9]+)/$', ResponseCreateAPIView.as_view(), name="conversation_api_view_parent_id_post"),
    url(r'^conversation-end/$', ConversationEndAPIView.as_view(), name='conversation_end_api_view'),
]