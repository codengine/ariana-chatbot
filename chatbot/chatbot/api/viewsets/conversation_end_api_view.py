import logging
import json
from rest_framework.response import Response
from rest_framework import status
from core.api.viewsets.base_api_view import BaseAPIView
from chatbot.loggers.ariana_message_formatter import ArianaLogMessageFormatter
from chatbot.models.chat import Chat


logger = logging.getLogger("chatbot")


class ConversationEndAPIView(BaseAPIView):
    http_method_names = ["post"]

    def post(self, request, format=None, **kwargs):
        response = {}

        try:
            post_data = request.data
            client_id = post_data.get('client_id')
            base_question_id = post_data.get('base_question_id')

            message = ""
            if client_id and base_question_id:
                chat_object = Chat.objects.filter(client_id=client_id, first_question_id=base_question_id, previous__isnull=True).first() #.order_by("id")
                question_text = chat_object.first_question.text
                answers = []
                chat_objects = Chat.objects.filter(client_id=client_id, first_question_id=base_question_id).order_by("id")
                for instance in chat_objects:
                    # if instance.valid_response:
                    answers += [instance.answer]

                message = question_text + " : " + (" -> ".join(answers))

            with ArianaLogMessageFormatter(logger=logger) as log:
                log.print_message_to_console(message)

            Chat.objects.filter(client_id=client_id, first_question_id=base_question_id).delete()

        except Exception as exp:
            logger.info("Exception: %s" % str(exp))

        return Response(response, status=status.HTTP_200_OK)