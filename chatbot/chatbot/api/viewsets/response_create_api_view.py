import logging
from rest_framework import status
from rest_framework.response import Response
from chatbot.models.conversation import Conversation
from core.api.viewsets.base_create_api_view import BaseCreateAPIView
from chatbot.api.serializers.chat_serializer import ChatSerializer


logger = logging.getLogger("chatbot")


class ResponseCreateAPIView(BaseCreateAPIView):
    queryset = Conversation.objects.all()
    serializer_class = ChatSerializer