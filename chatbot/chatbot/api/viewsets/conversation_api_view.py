import logging
from rest_framework import status
from rest_framework.response import Response
from chatbot.models.conversation import Conversation
from core.api.viewsets.base_retrieve_api_view import BaseRetrieveAPIView
from chatbot.api.serializers.conversation_serializer import ConversationSerializer
from chatbot.models.chat import Chat


logger = logging.getLogger("chatbot")


class ConversationAPIView(BaseRetrieveAPIView):
    queryset = Conversation.objects.all()
    serializer_class = ConversationSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')
        text = self.request.GET.get('text')
        client_id = self.request.GET.get('client_id')
        if not text:
            instance = Conversation.objects.filter(id=pk).first()
            if not instance:
                logger.info("Conversation not found with pk: %s" % pk)
            else:
                if client_id:
                    try:
                        Chat.objects.filter(client_id=client_id, first_question_id=pk).delete()
                    except Exception as exp:
                        pass
            return instance
        instance = Conversation.objects.filter(text__iexact=text, parent_id=pk).first()
        if not instance:
            logger.info("Conversation not found with pk: %s and text: %s" % (pk, text))
        # Find the next item
        if instance:
            return Conversation.objects.filter(parent_id=instance.pk).first()
        return instance

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        has_next = False
        message = None
        if not instance:
            pk = self.kwargs.get('pk')
            text = self.request.GET.get('text')

            if not Conversation.objects.filter(id=pk).exists():
                message = "Invalid id: %s" % pk
            else:
                c_instance = Conversation.objects.filter(id=pk).first()
                has_next = c_instance.has_next()
                if has_next:
                    message = "%s is not the correct option" % text
            response = {
                'has_next': has_next,
                'message': message,
                'object': None
            }
            return Response(response, status=status.HTTP_200_OK)

        serializer = self.get_serializer(instance)
        response = {
            'has_next': instance.has_next(),
            'message': message,
            'object': serializer.data
        }
        return Response(response, status=status.HTTP_200_OK)