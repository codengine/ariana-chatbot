from chatbot.models.conversation import Conversation
from chatbot.api.serializers.conversation_serializer import ConversationSerializer
from core.api.viewsets.base_list_api_view import BaseListAPIView


class ConversationListAPIView(BaseListAPIView):
    queryset = Conversation.objects.filter(parent_id__isnull=True)
    serializer_class = ConversationSerializer
