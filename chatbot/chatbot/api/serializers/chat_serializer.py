import logging
from core.api.serializers.base_model_serializer import BaseModelSerializer
from rest_framework import serializers
from chatbot.models.conversation import Conversation
from chatbot.models.chat import Chat


logger = logging.getLogger("chatbot")


class ChatSerializer(BaseModelSerializer):
    client_id = serializers.CharField()
    first_question_id = serializers.IntegerField()
    conversation_id = serializers.IntegerField()

    def is_valid(self, raise_exception=True):
        data = self._kwargs['data']
        logger.info(data)
        if not data.get('client_id'):
            return False
        first_question_id = data.get('first_question_id')
        try:
            first_question_id = int(first_question_id)
            if not Conversation.objects.filter(pk=first_question_id, parent__isnull=True).exists():
                return False
        except Exception as exp:
            return False

        conversation_id = data.get('conversation_id')
        try:
            conversation_id = int(conversation_id)
            if not Conversation.objects.filter(pk=conversation_id).exists():
                return False
        except Exception as exp:
            return False

        answer = data.get('answer')
        if not answer:
            return False

        if not Conversation.objects.filter(parent_id=conversation_id, text__iexact=answer).exists():
            return False

        valid = super(ChatSerializer, self).is_valid(raise_exception=raise_exception)
        return valid

    def create(self, validated_data):
        chat_instance = Chat()
        # {'client_id': '12345', 'first_question_id': 50, 'conversation_id': 51, 'answer': 'Yes'}
        client_id = validated_data['client_id']
        first_question_id = validated_data['first_question_id']
        conversation_id = validated_data['conversation_id']
        answer = validated_data['answer']
        chat_instance.client_id = client_id
        chat_instance.first_question_id = first_question_id
        chat_instance.conversation_id = conversation_id
        chat_instance.answer = answer
        conversation_instance = Conversation.objects.get(pk=conversation_id)
        if conversation_instance.parent:
            chat_instance.previous_id = conversation_instance.parent.pk
        next_instance = Conversation.objects.filter(parent_id=conversation_id, text__iexact=answer).first()
        if next_instance:
            chat_instance.next = next_instance
        status = None
        if next_instance:
            next_instance = Conversation.objects.filter(parent_id=next_instance.pk).first()
            if not next_instance:
                status = "COMPLETED"
        chat_instance.status = status
        chat_instance.save()
        return chat_instance

    class Meta:
        model = Chat
        fields = ('id', 'client_id', "first_question_id", "conversation_id", "answer")