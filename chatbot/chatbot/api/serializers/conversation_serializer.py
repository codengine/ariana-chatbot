from core.api.serializers.base_model_serializer import BaseModelSerializer
from chatbot.models.conversation import Conversation


class ConversationSerializer(BaseModelSerializer):

    def create(self, validated_data):
        return Conversation.objects.create(**validated_data)

    class Meta:
        model = Conversation
        fields = ('id', 'text', "parent_id")